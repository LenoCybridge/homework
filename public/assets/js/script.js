$(document).ready(function(){
     // Handle choose city,district,ward
   $('body').delegate('#like','click',function(e){
        var postId = $(this).attr('pid');
        var token = $('meta[name=csrf-token]').attr('content');
        $.ajax({
            url: "/client/like",
            type: 'GET',
            data: {
                postId: postId,
                _token: token
            },
            beforeSend: function(xhr) {
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            }
        }).done(function(res){
           if(res == 1){
               $('#like').addClass('text-primary').removeClass('text-dark');
           }else{
               $('#like').addClass('text-dark').removeClass('text-primary');
           }
        });
    });
}); 